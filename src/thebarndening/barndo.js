var the_barndo = 0;

$(function () {
  doTheBarndo();
  window.setInterval(doTheBarndo, 3000);
});

function doTheBarndo() {
  var number_of_barndos = 7;
  var new_barndo = the_barndo;
  while (new_barndo == the_barndo) {
    new_barndo = Math.floor(Math.random() * number_of_barndos);
  }
  the_barndo = new_barndo;

  $("#barndo").css("background-image", "url(/barndos/" + getBarndo(the_barndo) + ".png)");
  var max_top = $("body").height() - $("#barndo").outerHeight() + 1;
  var max_left = $("body").width() - $("#barndo").outerWidth() + 1;
  var top = Math.floor(Math.random() * max_top);
  var left = Math.floor(Math.random() * max_left);
  $("#barndo").css("top", top + "px");
  $("#barndo").css("left", left + "px");
}

function getBarndo(num) {
  var barndos = ["surprise", "party", "rage", "mustache", "disappointed", "disturbing", "default"];
  return barndos[num];
}
